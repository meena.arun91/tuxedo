### `npm install`

Install all the applications dependencies

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

Project Description

As an admin, I want to receive feedback from my users through a form in the application, So that I can better keep on top of monitoring and addressing feedback.

A simple form with textarea has been created with functional components as a FeedbackPage and for state management have used hooks.

A child component ListOfComments have been used to display the comments entered by the user.




