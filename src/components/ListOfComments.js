function ListOfComments({comments}) {
    return(
      <ul>
      {comments && comments.map((res,index) => {
        return <li key={index}>{res}</li>
      })
      }
      </ul>
    );
}

export default ListOfComments;