import {useState} from 'react';
import '../App.css';

import ListOfComments from './ListOfComments';
function FeedbackPage() {
    const [feedback, updatefeedback] = useState({value:'', submitDisabled: true, comments: []});
    function handleChange(e) {
      e.preventDefault();
      feedback.comments.push(feedback.value);
      updatefeedback({...feedback});
    }
  return (
    <form className="container">
      <div>
        <label>Please provide your valuable feedback here : </label>
        <textarea
          className="form__field--textarea"
          value={feedback.value}
          rows={5}
          onChange={ e => updatefeedback({...feedback,value: e.target.value, submitDisabled: false}) }
        />
      </div>
      <div>
        <input type="submit" className="form__field--button margin__top--medium" onClick={handleChange} disabled={feedback.submitDisabled} value="Submit" />
      </div>
      {feedback.comments.length !== 0 &&
      <>
      <div class="margin__top--medium">The feedbacks received so far:</div>
      <ListOfComments comments={feedback.comments} />
       </> 
      }
    </form>
  );
}

export default FeedbackPage;
