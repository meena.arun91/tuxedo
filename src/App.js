import FeedbackPage from './components/FeedbackPage'
import './App.css';

function App() {
  return (
    <div className="deck">
      <h1>Welcome to Tuxedo</h1>
      <FeedbackPage />
    </div>
  );
}

export default App;
